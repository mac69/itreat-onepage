<!DOCTYPE html>
<html lang="en">
    <?php include './src/layout/head.php';?>
    <body>
        <div class="swiper-container">
            <div class="nav-collapse collapse navbar-collapse" id="navbarSupportedContent"></div>
            <!-- Header nav -->
            <?php include './src/layout/menu.php';?>
            <!-- Header nav end -->
            <!-- Social nav -->
            <div class="social-web">
                <?php include './src/layout/socials.php';?>
            </div>
            <!-- Social nav end-->
            <div class="swiper-wrapper">
                <!-- Business solution -->
                <?php include './src/pages/business-solution.html';?>
                <!-- Business solution end-->
                <!-- Big 50 -->
                <?php include './src/pages/big-fifty.html';?>
                <!-- Big 50 end -->
                <!-- Giver -->
                <?php include './src/pages/giver.html';?>
                <!-- Giver end-->
                <!-- Guaranteed Sales -->
                <?php include './src/pages/guaranted-sales.html';?>
                <!-- Guaranteed Sales end -->
                <!-- Online Presence -->
                <?php include './src/pages/online-presence.html';?>
                <!-- Online Presence end -->
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <!-- Signup Modal -->
        <?php include './src/layout/signup-modal.php';?>
        <!-- Signup Modal end -->
        <?php include './src/layout/scripts.php';?>
    </body>
</html>