<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5e31765c8e78b86ed8aba0d6/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script> -->
<!--End of Tawk.to Script-->
<script>
    var swiper = new Swiper('.swiper-container', {
        mousewheel: true,
        keyboard: true,
        pagination: {
            el: '.swiper-pagination',
        },
        hashNavigation: {
            watchState: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    swiper.on('slideChangeTransitionEnd', function () {
        var url = window.location.href;
        var activeTab = url.substring(url.indexOf("#") + 1);
        removeActiveNav();
        mediaQuery();
        $("#" + activeTab).addClass("active");
    });

    $('input').on('touchstart mousedown', function(e){
        e.stopPropagation()
    })

    $(".nav-link").on("click", function() {
        removeActiveNav()
        $(this).addClass("active");
    });

    function removeActiveNav() {
        $(".nav-link").removeClass("active");
    }

    $(window).resize(function(){
        mediaQuery();
    });

    $(window).ready(function(){
        mediaQuery();
    });

    function mediaQuery() {
        var slideIndexArr= [2, 4, 6, 8]
        if ($(window).width() <= 990) { 
            $("#business-solution-col").addClass("d-none");
            $("#business-solution-subscribe-button").removeClass("d-none");
            $("#business-solution-subscribe-button").addClass("d-flex");
            $("#big-fifty-col").addClass("d-none");
            $("#giver-col").addClass("d-none");
            $("#guaranteed-sales-col").addClass("d-none");
            $("#online-presence-col").addClass("d-none");

            if(!$("#big-fifty-desctription").length) {
                swiper.addSlide(2, [
                    `<?php include './src/pages/big-fifty-description.html';?>`
                ]);
            }
            if(!$("#giver-bogo-description").length) {
                swiper.addSlide(4, [
                    `<?php include './src/pages/giver-description.html';?>`
                ]);
            }
            if(!$("#guaranted-sales-description").length) {
                swiper.addSlide(6, [
                    `<?php include './src/pages/guaranted-sales-description.html';?>`
                ]);
            }
            if(!$("#online-presence-description").length) {
                swiper.addSlide(8, [
                    `<?php include './src/pages/online-presence-description.html';?>`
                ]);
            }
        } else {
            $("#business-solution-col").removeClass("d-none");
            $("#business-solution-subscribe-button").addClass("d-none");
            $("#business-solution-subscribe-button").removeClass("d-flex");
            $("#big-fifty-col").removeClass("d-none");
            $("#giver-col").removeClass("d-none");
            $("#guaranteed-sales-col").removeClass("d-none");
            $("#online-presence-col").removeClass("d-none");

            if($("#big-fifty-desctription").length > 0) {
                swiper.removeSlide(slideIndexArr);
            }
            if($("#giver-bogo-description").length > 0) {
                swiper.removeSlide(slideIndexArr);
            }
            if($("#guaranted-sales-description").length > 0) {
                swiper.removeSlide(slideIndexArr);
            }
            if($("#online-presence-description").length > 0) {
                swiper.removeSlide(slideIndexArr);
            }
        }  
        
        swiper.update()

        $(".submit-btn-signup").click(function() {
            $(".signup").removeClass("d-none");
            $(".success-signup").addClass("d-none");
            $(".business-solution-banner-signup-card").addClass("max-width");
            $(".business-solution-banner-signup-card").addClass("max-width");
        });

        $(".signup-btn").click(function() {
            $(".signup").addClass("d-none");
            $(".success-signup").removeClass("d-none");
            $(".business-solution-banner-signup-card").addClass("w-100");
            $(".business-solution-banner-signup-card").removeClass("max-width");
        });
    }
    
</script>