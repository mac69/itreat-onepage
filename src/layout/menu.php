<nav class="navbar fixed-top navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#">
        <img src="./public/images/logo.png" alt="" class="img-fluid w-75">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse no-transition" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item" data-toggle="collapse" data-target="#navbarSupportedContent">
                <a class="nav-link active" id="business-solution" href="#business-solution">Business Solution!</a>
            </li>
            <li data-toggle="collapse" data-target="#navbarSupportedContent">
                <a class="nav-link" id="big-fifty" href="#big-fifty">The Big 50</a>
            </li>
            <li data-toggle="collapse" data-target="#navbarSupportedContent">
                <a class="nav-link" id="giver-bogo" href="#giver-bogo">5-45% Giver & BOGO</a>
            </li>
            <li data-toggle="collapse" data-target="#navbarSupportedContent">
                <a class="nav-link" id="guaranteed-sales" href="#guaranteed-sales">Guaranteed Sales</a>
            </li>
            <li data-toggle="collapse" data-target="#navbarSupportedContent">
                <a class="nav-link" id="online-presence" href="#online-presence">Online Presence</a>
            </li>
        </ul>
        <div class="social-mobile">
            <?php include './src/layout/socials.php';?>
        </div>
    </div>
</nav>