<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg justify-content-center" role="document">
        <div class="modal-content business-solution-banner-signup-card max-width">
            <div class="card p-4">
                <div class="card-body">
                    <div class="signup">
                        <h5 class="text-center font-weight-bold">Get in touch with us</h5>
                        <form class="py-4">
                            <div class="form-group">
                                <input type="text" placeholder="Tell us your company name?" class="form-control business-solution-signup-input">
                            </div>
                            <div class="form-group">
                                <select class="form-control business-solution-signup-input" placeholder="Which industry you belong?">
                                    <option>Which industry you belong?</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Email Address" class="form-control business-solution-signup-input">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Mobile Number" class="form-control business-solution-signup-input">
                            </div>
                            <button type="submit" class="btn btn btn-outline-dark d-flex mx-auto mt-5 rounded-pill px-5 font-weight-bold signup-btn" data-toggle="modal" data-target="#successModal">Let's Go!</button>
                        </form>
                    </div>
                    <div class="success-signup d-none py-5">
                        <div class="d-flex justify-content-center">
                            <img src="./public/images/great-job.png" width="330" alt="" class="img-fluid my-3">
                        </div>
                        <p class="text-center fs-21 fw-600">Now its our turn, one of our awesome <br/>
                            agent will get back to you and we’ll handle the rest <br/>
                            meanwhile check your email.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>